# -*- encoding: utf-8 -*-
#!/usr/bin/env python
'''
@File    : jieba_seg.py
@Time    : 2021/05/27 18:37:34
@Author  : Parker
@Version : 1.0
@Contact : 1251633579@qq.com
@Desc    : 
jieba:基于uni-gram语言模型和基于HMM的分词方法
内部原理：
基于前缀词典实现高效的词图扫描，生成句子中汉字所有可能成词情况所构成的有向无环图 (DAG)
采用了动态规划查找最大概率路径, 找出基于词频的最大切分组合
对于未登录词，采用了基于汉字成词能力的 HMM 模型，使用了 Viterbi 算法
'''
# Lib Config: If custom packages will be used, Please add the following!
# import os, sys
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(BASE_DIR)

# %% 生成前缀词典
import re
import sys
import math
import pickle
from math import log
import random

with open("./jieba_resource/dict.txt", encoding='utf-8') as f:
    lines = f.readlines()
    for line in random.choices(lines, k=5):
        print(line)
# %% 生成前缀词典
def get_prefix_dict(f_name):
    lfreq = {}
    ltotal = 0
    f = open(f_name, encoding='utf-8')
    for lineno, line in enumerate(f, 1):
        try:
            line = line.strip()
            word, freq = line.split(' ')[:2]
            freq = int(freq)
            lfreq[word] = freq
            ltotal += freq
            for ch in range(len(word)):
                wfrag = word[:ch + 1]
                if wfrag not in lfreq:
                    lfreq[wfrag] = 0
        except ValueError:
            raise ValueError(
                'invalid dictionary entry in %s at Line %s: %s' % (f_name, lineno, line))
    f.close()
    return lfreq, ltotal

freq, total = get_prefix_dict("./jieba_resource/dict.txt")
print("生成前缀词典的大小为{}。".format(total))
# %% 构造DAG
def get_DAG(sentence, freq):
    DAG = {}
    N = len(sentence)
    for k in range(N):
        tmplist = []
        i = k
        frag = sentence[k]
        while i < N and frag in freq:
            if freq[frag]:
                tmplist.append(i)
            i += 1
            frag = sentence[k:i + 1]
        if not tmplist:
            tmplist.append(k)
        DAG[k] = tmplist
    return DAG

example = "我在学习结巴分词"
dag = get_DAG(example, freq)
print(dag)
# %% 动态规划找到最大路径
def clac(sentence, DAG, freq, total):
    n = len(sentence)
    route = {n: (0, 0)}
    log_total = log(total)

    for i in range(n-1, -1, -1):
        cache = []
        for j in DAG[i]:
            log_p = log(freq.get(sentence[i:j+1], 0) or 1)
            cache.append((log_p - log_total + route[j+1][0], j))
        route[i] = max(cache)
    return route

route = clac(example, dag, freq, total)
print(route)
# %% no_hmm分词
def cut_no_hmm(sentence, route):
    i = 0
    while(i < len(route)-1):
        j = route[i][1]
        yield sentence[i:j+1]
        i = j + 1

for word in cut_no_hmm(example, route):
    print(word)
# %% no-hmm无法解决未登录词
example = "韩冰是个好人"
dag = get_DAG(example, freq)
route = clac(example, dag, freq, total)
list(cut_no_hmm(example, route))
# %%
prob_start = pickle.load(
    open("./jieba_resource/finalseg/prob_start.p", "rb"))  # 初始概率参数
prob_emit = pickle.load(
    open("./jieba_resource/finalseg/prob_emit.p", "rb"))  # 发射概率
prob_trans = pickle.load(
    open("./jieba_resource/finalseg/prob_trans.p", "rb"))  # 状态转移概率
# %% 初始概率
{key: math.exp(value) for key, value in prob_start.items()}
# %% 状态转移概率
{key: {k: math.exp(v) for k, v in value.items()}
 for key, value in prob_trans.items()}
# %% 发射概率
{key: {k: math.exp(v) for i, (k, v) in enumerate(value.items()) if i < 5}
 for key, value in prob_emit.items()}
# %% 概率转移
MIN_FLOAT = -3.14e100
start_2_B = prob_emit["B"].get("韩", MIN_FLOAT) + prob_start["B"]
start_2_E = prob_emit["E"].get("韩", MIN_FLOAT) + prob_start["E"]
start_2_M = prob_emit["M"].get("韩", MIN_FLOAT) + prob_start["M"]
start_2_S = prob_emit["S"].get("韩", MIN_FLOAT) + prob_start["S"]

print(start_2_B, start_2_E, start_2_M, start_2_S)
# %% 维特比算法-hmm解码
def viterbi(obs, states, start_p, trans_p, emit_p):
    V = [{}]  # tabular
    path = {}
    for y in states:  # init
        V[0][y] = start_p[y] + emit_p[y].get(obs[0], MIN_FLOAT)
        path[y] = [y]
    for t in range(1, len(obs)):
        V.append({})
        newpath = {}
        for y in states:
            em_p = emit_p[y].get(obs[t], MIN_FLOAT)
            (prob, state) = max(
                [(V[t - 1][y0] + trans_p[y0].get(y, MIN_FLOAT) + em_p, y0) for y0 in states])
            V[t][y] = prob
            newpath[y] = path[state] + [y]
        path = newpath

    (prob, state) = max((V[len(obs) - 1][y], y) for y in 'ES')

    return (prob, path[state])

example = "韩冰是个"
prob, path = viterbi(example, "BEMS", prob_start, prob_trans, prob_emit)

for w, s in zip(example, path):
    print(w, "->", s)
# %% 基于hmm识别未登录词
def hmm(sentence, start_P, trans_P, emit_P):
    prob, pos_list = viterbi(sentence, 'BMES', start_P, trans_P, emit_P)
    begin, nexti = 0, 0
    # print pos_list, sentence
    for i, char in enumerate(sentence):
        pos = pos_list[i]
        if pos == 'B':
            begin = i
        elif pos == 'E':
            yield sentence[begin:i + 1]
            nexti = i + 1
        elif pos == 'S':
            yield char
            nexti = i + 1
    if nexti < len(sentence):
        yield sentence[nexti:]

def cut_hmm(sentence):
    dag = get_DAG(sentence, freq)
    route = clac(sentence, dag, freq, total)
    i = 0
    buf = ""
    while(i < len(route)-1):
        j = route[i][1] + 1

        if j - i <= 1:
            buf += sentence[i]
        else:
            if buf:
                if len(buf) == 1:
                    yield buf
                else:
                    if buf not in freq:
                        for w in hmm(buf, prob_start, prob_trans, prob_emit):
                            yield w
                    else:
                        for w in buf:
                            yield w
                buf = ""
            yield sentence[i:j]
        i = j

    if buf:
        if len(buf) == 1:
            yield buf
            buf = ""
        else:
            if buf not in freq:
                for w in hmm(buf, prob_start, prob_trans, prob_emit):
                    yield w
            else:
                for w in buf:
                    yield w

example = "韩冰是个好人"
for word in cut_hmm(example):
    print(word)
# %% 正则表达式辅助分词
sentences = ["最终结果为1920.2333", "今天是2020.9.2", "Apple手机是我的最爱"]
for s in sentences:
    print(list(cut_hmm(s)))

# 用于提取连续的汉字部分
re_han = re.compile("([\u4E00-\u9FD5]+)")
# 用于分割连续的非汉字部分
re_skip = re.compile("([a-zA-Z0-9\.]+(?:\.\d+)?%?)")

def cut_regx_hmm(sentence):
    blocks = re_han.split(sentence)
    for block in blocks:
        if not block:
            continue
        if re_han.match(block):
            yield from cut_hmm(block)
        else:
            for ss in re_skip.split(block):
                if ss:
                    yield ss

for s in sentences:
    print(list(cut_regx_hmm(s)))